# MarkHugo

Script for converting good markdown structure in order to make Hugo Static Site generator (SSG) read for use.

For detail, See [here](https://blog.ummit.dev/posts/web/ssg/hugo/merge-into-good-markdown-structure/).

>Note: LINUX ONLY. CUZ THIS IS BASH SCRIPT!

## Usage

Place this script in your article (markdown) directory and run the script.

```shell
git clone https://codeberg.org/UmmIt/Markhugo && cd Markhugo/script && chmod +x *.sh

cp -v *.sh ~/Documents/<Article_paths>
```

> IMPORTANT: Afer done the process, Please check out the files, is it what you expected? Since this script might be overwrite your current file.
